package com.gdstruc.module4;

import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        queueArray matchQueue = new queueArray(50);
        System.out.println("Players");
        matchQueue.add(new Player(1, "Player(A)" , 50));
        matchQueue.add(new Player(2, "Player(B)" , 49));
        matchQueue.add(new Player(3, "Player(C)" , 48));
        matchQueue.add(new Player(4, "Player(D)" , 47));
        matchQueue.add(new Player(5, "Player(E)" , 46));
        matchQueue.add(new Player(6, "Player(F)" , 45));
        matchQueue.add(new Player(7, "Player(G)" , 45));
        matchQueue.add(new Player(8, "Player(H)" , 43));
        matchQueue.add(new Player(9, "Player(I)" , 42));
        matchQueue.add(new Player(10, "Player(J)" , 41));
        matchQueue.add(new Player(11, "Player(K)" , 40));
        matchQueue.add(new Player(12, "Player(L)" , 39));
        matchQueue.add(new Player(13, "Player(M)" , 38));
        matchQueue.add(new Player(14, "Player(N)" , 37));
        matchQueue.add(new Player(15, "Player(O)" , 36));
        matchQueue.add(new Player(16, "Player(P)" , 35));
        matchQueue.add(new Player(17, "Player(Q)" , 34));
        matchQueue.add(new Player(18, "Player(R)" , 33));
        matchQueue.add(new Player(19, "Player(S)" , 32));
        matchQueue.add(new Player(20, "Player(T)" , 31));
        matchQueue.add(new Player(21, "Player(U)" , 30));
        matchQueue.add(new Player(22, "Player(V)" , 29));
        matchQueue.add(new Player(23, "Player(W)" , 28));
        matchQueue.add(new Player(24, "Player(X)" , 27));
        matchQueue.add(new Player(25, "Player(Y)" , 26));
        matchQueue.add(new Player(26, "Player(Z)" , 25));
        matchQueue.add(new Player(27, "Player(AA)" , 24));
        matchQueue.add(new Player(28, "Player(AB)" , 23));
        matchQueue.add(new Player(29, "Player(AC)" , 22));
        matchQueue.add(new Player(30, "Player(AD)" , 21));
        matchQueue.add(new Player(31, "Player(AE)" , 20));
        matchQueue.add(new Player(32, "Player(AF)" , 19));
        matchQueue.add(new Player(33, "Player(AG)" , 18));
        matchQueue.add(new Player(34, "Player(AH)" , 17));
        matchQueue.add(new Player(35, "Player(AI)" , 16));
        matchQueue.add(new Player(36, "Player(AJ)" , 15));
        matchQueue.add(new Player(37, "Player(AK)" , 14));
        matchQueue.add(new Player(38, "Player(AL)" , 13));
        matchQueue.add(new Player(39, "Player(AM)" , 12));
        matchQueue.add(new Player(40, "Player(AN)" , 11));
        matchQueue.add(new Player(41, "Player(AO)" , 10));
        matchQueue.add(new Player(42, "Player(AP)" , 9));
        matchQueue.add(new Player(43, "Player(AQ)" , 8));
        matchQueue.add(new Player(44, "Player(AR)" , 7));
        matchQueue.add(new Player(45, "Player(AS)" , 6));
        matchQueue.add(new Player(46, "Player(AT)" , 5));
        matchQueue.add(new Player(47, "Player(AU)" , 4));
        matchQueue.add(new Player(48, "Player(AV)" , 3));
        matchQueue.add(new Player(49, "Player(AW)" , 2));
        matchQueue.add(new Player(50, "Player(AX)" , 1));
        System.out.println("=========================================");
        matchQueue.queuePrint();
        System.out.println("=========================================");

        int matchStarted = 0;
        int currentPlayer = 0;

        Random rand = new Random();
        int upperbound = 7;

        System.out.println("Press Enter to start the server . . .");
        new java.util.Scanner(System.in).nextLine();

        while (matchStarted != 10){
            int playerQ = (rand.nextInt(upperbound)) + 1;
            currentPlayer = currentPlayer + playerQ;

            if (currentPlayer > 4){
                System.out.println("Matches Started: " + matchStarted);
                System.out.println("Players that just queued: " + playerQ);
                System.out.println("Players in queue: " + currentPlayer);
                System.out.println("5 players detected . . . Match Found . . .");
                for (int i = 0; i < 5; i++){
                    System.out.println("Joined Game: " + matchQueue.remove());
                }
                matchStarted++;
                currentPlayer = currentPlayer - 5;
                System.out.println("Players in queue after match found: " + currentPlayer);
                System.out.println("Press Enter to proceed. . .");
                new java.util.Scanner(System.in).nextLine();
                if (currentPlayer > 4){
                    System.out.println("5 players detected . . . Match Found . . .");
                    for (int i = 0; i < 5; i++){
                        System.out.println("Joined Game: " + matchQueue.remove());
                    }
                    matchStarted++;
                    currentPlayer = currentPlayer - 5;
                    System.out.println("Players in queue after match found: " + currentPlayer);
                    System.out.println("Press Enter to proceed. . .");
                    new java.util.Scanner(System.in).nextLine();
                }
            }
            else{
                System.out.println("Matches Started:" + matchStarted);
                System.out.println("Players that just queued: " + playerQ);
                System.out.println("Players in queue: " + currentPlayer);
                System.out.println("Not enough Players to start Match. . . Press Enter to proceed. . .");
                new java.util.Scanner(System.in).nextLine();
            }
        }
        System.out.println("10 Matches have been made. . . Server Shutting Down. . .");
    }
}
