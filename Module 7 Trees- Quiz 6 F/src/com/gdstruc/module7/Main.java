package com.gdstruc.module7;

public class Main {

    public static void main(String[] args) {
        Tree tree = new Tree();

        tree.insert(25);
        tree.insert(17);
        tree.insert(29);
        tree.insert(10);
        tree.insert(16);
        tree.insert(-5);
        tree.insert(60);
        tree.insert(55);

        System.out.println("=========================================");
        System.out.println("Traverse in Order (Ascending): ");
        tree.traverseInOrder();
        System.out.println("=========================================");
        System.out.println("Traverse in Order (Descending): ");
        tree.traverseInDescending();
        System.out.println("=========================================");
        System.out.println("Print Maximum Value: ");
        System.out.println("Data: " + tree.findMax(tree.getMax().getRightChild()));
        System.out.println("=========================================");
        System.out.println("Print Minimum Value: ");
        System.out.println("Data: " + tree.findMin(tree.getMin().getLeftChild()));
        System.out.println("=========================================");
        System.out.println(tree.get(25));
    }
}
