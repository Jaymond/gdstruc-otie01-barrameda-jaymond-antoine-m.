package com.gdstruc.module7;

public class Tree {

    private Node root;

    public void insert(int value){
        if (root == null){
            root = new Node(value);
        }
        else{
            root.insert(value);
        }
    }

    public void traverseInOrder(){
        if (root != null){
            root.traverseInOrder();
        }
    }

    public Node get(int value)
    {
        if (root != null)
        {
            return root.get(value);
        }
        return null;
    }

    public void traverseInDescending(){
        if (root != null){
            root.traverseInDescending();
        }
    }

    public Node getMin(){
        if (root != null){
            return root.getMin();
        }
        return null;
    }

    public Node getMax(){
        if (root != null){
            return root.getMax();
        }
        return null;
    }

    static int findMax(Node node)
    {
        if (node == null)
            return Integer.MIN_VALUE;

        int res = node.data;
        int lres = findMax(node.getLeftChild());
        int rres = findMax(node.getRightChild());

        if (lres > res)
            res = lres;
        if (rres > res)
            res = rres;
        return res;
    }

    static int findMin(Node node)
    {
        if (node == null)
            return Integer.MAX_VALUE;

        int res = node.data;
        int lres = findMin(node.getLeftChild());
        int rres = findMin(node.getRightChild());

        if (lres < res)
            res = lres;
        if (rres < res)
            res = rres;
        return res;
    }
}
