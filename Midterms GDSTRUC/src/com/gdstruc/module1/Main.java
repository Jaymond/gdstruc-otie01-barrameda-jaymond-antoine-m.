package com.gdstruc.module1;

import java.util.Random;

public class Main{

    public static void main(String[] args){

        //Deck of Cards
        CardStack deckStack = new CardStack();
        //Discarded Cards
        CardStack discardedStack = new CardStack();
        //Player Cards
        CardStack holdStack = new CardStack();

        //Cards in the deck
        deckStack.push(new Card("Clover", 1));
        deckStack.push(new Card("Clover", 2));
        deckStack.push(new Card("Clover", 3));
        deckStack.push(new Card("Clover", 4));
        deckStack.push(new Card("Clover", 5));
        deckStack.push(new Card("Clover", 6));
        deckStack.push(new Card("Clover", 7));
        deckStack.push(new Card("Spade", 1));
        deckStack.push(new Card("Spade", 2));
        deckStack.push(new Card("Spade", 3));
        deckStack.push(new Card("Spade", 4));
        deckStack.push(new Card("Spade", 5));
        deckStack.push(new Card("Spade", 6));
        deckStack.push(new Card("Spade", 7));
        deckStack.push(new Card("Heart", 1));
        deckStack.push(new Card("Heart", 2));
        deckStack.push(new Card("Heart", 3));
        deckStack.push(new Card("Heart", 4));
        deckStack.push(new Card("Heart", 5));
        deckStack.push(new Card("Heart", 6));
        deckStack.push(new Card("Heart", 7));
        deckStack.push(new Card("Diamond", 1));
        deckStack.push(new Card("Diamond", 2));
        deckStack.push(new Card("Diamond", 3));
        deckStack.push(new Card("Diamond", 4));
        deckStack.push(new Card("Diamond", 5));
        deckStack.push(new Card("Diamond", 6));
        deckStack.push(new Card("Diamond", 7));
        deckStack.push(new Card("Joker", 1));
        deckStack.push(new Card("Joker", 2));

        //Randomizer
        Random Randomizer = new Random();
        //Round
        int Round = 1;
        while(!deckStack.empty()){
            //Random Number Generator 1-5
            int drawNumber;
            drawNumber = Randomizer.nextInt(5) + 1;
            System.out.println("=========================================================");
            System.out.println("Round: " + Round + "");
            System.out.println("Deck: " + deckStack.cardSize() + " card(s)");
            System.out.println("Discard Pile: " + discardedStack.cardSize() + " card(s)");
            System.out.println("Player Cards: " + holdStack.cardSize() + " card(s)");
            holdStack.printStack();
            System.out.println("=========================================================");
            System.out.println("Press Any Enter To Continue...");
            new java.util.Scanner(System.in).nextLine();

            //Random Action selection 1-3 (1) Draw, (2) Discard, (3) Recover
            int randomAction;
            randomAction = Randomizer.nextInt(3) + 1;
            if (randomAction == 1){
                if (drawNumber > deckStack.cardSize()){
                    drawNumber = deckStack.cardSize();
                }
                System.out.println("\n You drew " + drawNumber + " card(s)");
                for (int i = 0; i < drawNumber; i++){
                    Card pickedCard = deckStack.peek();
                    holdStack.push(pickedCard);
                    deckStack.pop();
                }
            }
            else if (randomAction == 2){
                if(drawNumber > holdStack.cardSize()){
                    drawNumber = holdStack.cardSize();
                }
                System.out.println("\nYou discarded " + drawNumber + " card(s)");
                for (int i = 0; i < drawNumber; i++){
                    Card discardedCard = holdStack.peek();
                    discardedStack.push(discardedCard);
                    holdStack.pop();
                }
            }
            else if (randomAction == 3){
                if(drawNumber > discardedStack.cardSize()){
                    drawNumber = discardedStack.cardSize();
                }

                System.out.println("\nYou recovered " + drawNumber + " card(s)");
                for (int i = 0; i < drawNumber; i++){
                    Card recoveredCard = discardedStack.peek();
                    holdStack.push(recoveredCard);
                    discardedStack.pop();
                }
            }
            Round++;
        }
        System.out.println("=========================================================");
        System.out.println("Round: " + Round + "");
        System.out.println("Deck: " + deckStack.cardSize() + " cards");
        System.out.println("Discard Pile: " + discardedStack.cardSize() + " cards");
        System.out.println("Player Cards: " + holdStack.cardSize() + " cards");
        holdStack.printStack();
        System.out.println("=========================================================");
        System.out.println("End. . . the deck is now empty.");
    }
}
