package com.gdstruc.module1;

import java.util.Objects;

public class Card{

    private  String suit;
    private int number;

    public Card(String suit, int number){
        this.suit = suit;
        this.number = number;
    }
    public String getSuit(){
        return suit;
    }
    public void setShape(String suit){
        this.suit = suit;
    }
    public int getNumber(){
        return number;
    }
    public void setNumber(int number){
        this.number = number;
    }
    @Override
    public String toString(){
        return "Card{" +
                "shape='" + suit + '\'' +
                ", number=" + number +
                '}';
    }
    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Card card = (Card) o;
        return number == card.number && Objects.equals(suit, card.suit);
    }
    @Override
    public int hashCode(){
        return Objects.hash(suit, number);
    }
}
