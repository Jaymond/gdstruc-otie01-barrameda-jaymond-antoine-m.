package com.gdstruc.module1;

public class PlayerLinkedList {

    private PlayerNode head;

    public void addToFront(Player player) {
        PlayerNode playerNode = new PlayerNode(player);
        playerNode.setNextPlayer(head);
        head = playerNode;
    }

    public void removeFront() {
        PlayerNode node = head;
        head = head.getNextPlayer();
        node.setNextPlayer(null);
    }


    public void printList() {
        PlayerNode current = head;
        System.out.print("HEAD -> ");
        while (current != null) {
            System.out.print(current.getPlayer());
            System.out.print(" -> ");
            current = current.getNextPlayer();
        }
        System.out.println("null");
    }

    public void counter() {
        int counter = 0;
        PlayerNode current = head;

        while (current != null) {
            current = current.getNextPlayer();
            counter++;

        }
        System.out.print("\nList Size: ");
        System.out.print(counter);
    }

    public boolean contain(Player player)
    {
        PlayerNode current = head;
        boolean check = false;
        while (current != null) {

            if (current.getPlayer() == player)
            {
                check = true;
            }
            current = current.getNextPlayer();
        }
        System.out.print("\nIs " + player.getName() + " in the Linked List: " + check + "");
        return check;
    }

    public void index(Player player)
    {

        PlayerNode current = head;

        int I = -1;
        int PlayerIndex = 0;
        while (current != null)
        {
            if (current.getPlayer() == player)
            {
                I = PlayerIndex;
                System.out.print("\n" + player.getName() + "'s Index in the Linked List is: " + I + "");
            }
            PlayerIndex++;
            current = current.getNextPlayer();
        }
        if(I == -1)
        {
            System.out.print("\n" + player.getName() + "'s Index is not in the Linked List: " + I + "");
        }
    }
}

