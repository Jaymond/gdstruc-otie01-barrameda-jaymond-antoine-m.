package com.gdstruc.module1;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {


        Player asuna = new Player(0, "Asuna", 100);
        Player lethalbacon = new Player(1, "LethalBacon", 205);
        Player hpDeskJet = new Player(2, "HPDeskJet", 34);
        Player dummy = new Player(3, "Dummy", 999);


        PlayerLinkedList playerLinkedList = new PlayerLinkedList();

        playerLinkedList.addToFront(asuna);
        playerLinkedList.addToFront(lethalbacon);
        playerLinkedList.addToFront(hpDeskJet);

        System.out.print("Current List: ");
        playerLinkedList.printList();


        playerLinkedList.removeFront();

        System.out.print("\nNew List: ");
        playerLinkedList.printList();

        System.out.print("\n");
        playerLinkedList.counter();

        System.out.print("\n");
        playerLinkedList.contain(dummy);

        System.out.print("\n");
        playerLinkedList.contain(asuna);

        System.out.print("\n================");
        System.out.print("\n");
        playerLinkedList.index(dummy);

        System.out.print("\n");
        playerLinkedList.index(asuna);

        System.out.print("\n");
        playerLinkedList.index(lethalbacon);

        System.out.print("\n");
        playerLinkedList.index(hpDeskJet);
//        System.out.println(playerList.get(1));

//        playerList.add(2, new Player(553, "Arctis", 55));
//
//        playerList.remove(2);

//        System.out.println(playerList.contains(new Player(2, "LethalBacon", 205)));
//     System.out.println(playerList.indexOf(new Player( 2, "LethalBacon", 205)));
        //        playerList.forEach(player -> System.out.println(player));
//        for (Player p : playerList)
//        {
//            System.out.println(p);
//        }
    }
}
