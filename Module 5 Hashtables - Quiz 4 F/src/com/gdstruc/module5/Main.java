package com.gdstruc.module5;

public class Main{
    public static void main(String[] args){
        Player Anthony = new Player(1, "Anthony", 1);
        Player Barrameda = new Player(2, "Barrameda", 2);
        Player Carrion = new Player(3, "Carrion", 3);
        Player Dima = new Player(4, "Dima", 4);
        Player Esther = new Player(5, "Esther", 5);

        Hashtable hashtable = new Hashtable();
        hashtable.put(Anthony.getUserName(), Anthony);
        hashtable.put(Barrameda.getUserName(), Barrameda);
        hashtable.put(Carrion.getUserName(), Carrion);
        hashtable.put(Dima.getUserName(), Dima);
        hashtable.put(Esther.getUserName(), Esther);



        System.out.println("===========================================");
        hashtable.printHashtable();
        System.out.println("===========================================");
        System.out.println("Print Player A");
        System.out.println(hashtable.get("Anthony"));
        System.out.println(hashtable.get("Carrion"));
        System.out.println("===========================================");
        System.out.println("Removed Player A")  ;
        System.out.println(hashtable.remove("Anthony"));
        System.out.println("===========================================");
        System.out.println("Print Player A");
        System.out.println(hashtable.get("Anthony"));
        System.out.println(hashtable.get("Carrion"));
        System.out.println("===========================================");
        hashtable.printHashtable();
        System.out.println("===========================================");


    }
}
